<p align="center">
<img src="https://telegra.ph/file/2d99afacf3184de8f9bd9.png" alt="SHIZIYAMA" width="500"/>


</p>
<p align="center">
<a href="#"><img title="Logo" src="https://img.shields.io/badge/SHIZIYAMA-green?colorA=%23ff0000&colorB=%23017e40&style=for-the-badge"></a>
</p>
<p align="center">
<a href="https://github.com/zeezxyz/fg-api"><img title="Author" src="https://img.shields.io/badge/Author-zeezxyz-red.svg?style=for-the-badge&logo=github"></a>
</p>
<p align="center">
<a href="https://github.com/zeezxyz"><img title="Followers" src="https://img.shields.io/github/followers/zeezxyz2803?color=blue&style=flat-square"></a>
<a href="https://github.com/zeezxyz/fg-api"><img title="Stars" src="https://img.shields.io/github/stars/zeezxyz/fg-api?color=red&style=flat-square"></a>
<a href="https://github.com/zeezxyz/fg-api/network/members"><img title="Forks" src="https://img.shields.io/github/forks/zeezxyz/fg-api?color=red&style=flat-square"></a>
<a href="https://github.com/zeezxyz/fg-api/watchers"><img title="Watching" src="https://img.shields.io/github/watchers/zeezxyz/fg-api?label=Watchers&color=blue&style=flat-square"></a>
</p>

---
## Deploy to Heroku
[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy?template=https://github.com/zeezxyz/fg-api)

## Follow Me
[![INSTAGRAM](https://img.shields.io/badge/Instagram-25D366?style=for-the-badge&logo=instgram&logoColor=white)](https://Instagram.com/ftditzzxy) 


---------


### Demo

<a href="https://api.shiziyama.ml">DEMO HERE</a>


<p align="center">
<img src="https://telegra.ph/file/9b92b4d0b1d7af41d1dc8.jpg" alt="FG API" width="500"/>
